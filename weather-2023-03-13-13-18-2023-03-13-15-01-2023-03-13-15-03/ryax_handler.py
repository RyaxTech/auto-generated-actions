import requests

def handle(mod_in):
    city = mod_in.get("city")
    url = f"http://api.openweathermap.org/data/2.5/weather?q={city}&appid=API_KEY"
    response = requests.get(url)
    data = response.json()
    weather = data["weather"][0]["description"]
    return {"weather": weather}
