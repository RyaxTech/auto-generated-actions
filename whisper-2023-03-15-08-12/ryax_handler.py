import subprocess

def handle(mod_in):
    video_file = mod_in.get("video_file")
    output_file = "/tmp/output.txt"
    subprocess.run(["whisper", video_file, "-o", output_file])
    with open(output_file, "r") as f:
        content = f.read()
    return {"text": content}
